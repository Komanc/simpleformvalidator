<?php

/**
 * Simple class for validation forms inputs
 * 
 * Needs started sessions
 *
 * @uses sessions 
 */
class SimpleFormValidator
{
    const RULE_REQUIRED  = 'required';
    const RULE_NUMERIC   = 'numeric';
    const RULE_EMAIL     = 'email';
    const RULE_CHECK     = 'check';
    const RULE_TELEPHONE = 'telephone';
    const RULE_EMPTY     = 'empty';

    /** @var array validation rules */
    private $rules  = array();
    
    /** @var array list of all supported validation types */
    private $supportedRules = array(
        self::RULE_REQUIRED, self::RULE_NUMERIC, self::RULE_EMAIL,
        self::RULE_CHECK, self::RULE_TELEPHONE, self::RULE_EMPTY,
    );
    
    /** @var array error messages */
    private $messages = array(
        self::RULE_REQUIRED  => 'Toto pole nesmí být prázdné.',
        self::RULE_NUMERIC   => 'V tomto poli mohou být jen čísla.',
        self::RULE_TELEPHONE => 'Toto pole smí obsahovat jen čísla a mezery.',
        self::RULE_EMAIL     => 'Nesprávný formát e-mailu.',
        self::RULE_EMPTY     => 'Detekován pokus o spam, formulář nebyl odeslán.',
        self::RULE_CHECK     => 'Toto pole musí být zaškrtnuto.',
    );

    /** @var string last validation rule to adding fields */
    private $lastAddedRule = '';
    
    /** @var string SimpleFormValidator session namespace */
    private $sessionNS = 'SimpleForm';
    
    /**
     * @param boolean $clear clear all SimpleFormValidator data
     */
    public function __construct($clear = false) 
    {
        if (!isset($_SESSION[$this->sessionNS])) {
            $_SESSION[$this->sessionNS] = array(
                'values' => array(),
                'errors' => array(),
                'isSent' => false,
            );
        } elseif ($clear) {
            $this->clear();
        }
    }

    /**
     * Return true if form was sent
     *
     * @return boolean
     */
    public function isSent()
    {
        if (!isset($_SESSION[$this->sessionNS]['isSent'])) {
            $_SESSION[$this->sessionNS]['isSent'] = false;
        }

        return $_SESSION[$this->sessionNS]['isSent'];

    }
    
    /**
     * Sets error message to given field
     * @param string $field field name from HTML
     * @param mixed $value
     * @return \SimpleFormValidator
     */
    public function setError($field, $value) 
    {
        $_SESSION[$this->sessionNS]['errors'][$field] = $value;
        return $this;
    }       
    
    /**
     * Sets value to given field
     * @param string $field field name from HTML
     * @param mixed $value
     * @return \SimpleFormValidator
     */    
    public function setValue($field, $value) 
    {
        $_SESSION[$this->sessionNS]['values'][$field] = $value;
        return $this;
    }    
    
    /**
     * Returns error message of given form field
     * @param string $field field name from HTML
     * @return mixed
     */       
    public function getError($field) 
    {
        if (!empty($_SESSION[$this->sessionNS]['errors'][$field])) {
            return $_SESSION[$this->sessionNS]['errors'][$field];
        }
        
        return '';
    }    
    
    /**
     * Returns list of all form errors
     * @return array 
     */
    public function getErrors()
    {
        return $_SESSION[$this->sessionNS]['errors'];
    }
    
    /**
     * Returns true if validation sets some errors
     * @return boolean 
     */
    public function hasError()
    {
        return ($this->isSent() && !empty($_SESSION[$this->sessionNS]['errors']));
    }

    /**
     * Returns value of given form field
     * @param string $field field name from HTML
     * @return mixed
     */           
    public function getValue($field) 
    {
        if (!empty($_SESSION[$this->sessionNS]['values'][$field])) {
            return $_SESSION[$this->sessionNS]['values'][$field];
        }
        
        return '';
    }

    /**
     * Check if value was selected.
     * @param string $field field name from HTML
     * @param string $value value to check
     * @param string $output (optional) output text which is returned instead of true
     * @return bool
     */
    public function isSelected($field, $value, $output = null)
    {
        $field = $this->normalizeFieldName($field);
        $field = $this->getValue($field);

        $result = (is_array($field) && in_array($value, $field));
        if ($result && is_string($output)) {
            return $output;
        }

        return $result;
    }
    
    /**
     * Returns list of all values which was sent from form
     * @return array 
     */
    public function getValues()
    {
        return $_SESSION[$this->sessionNS]['values'];
    }
   
    /**
     * Imports form validation rule set
     * @param array $rules 
     * @return \SimpleFormValidator
     */
    public function setRules(array $rules) 
    {
        $this->rules = $rules;
        return $this;
    }
    
    /**
     * Exports form validation rule set
     * @return array
     */
    public function getRules() 
    {
        return $this->rules;
    }
    
    /**
     * Adds validation rule type
     * 
     * @param string $type for supported validation rules see getSupportedRules method
     * @return \SimpleFormValidator
     * @see getSupportedRules
     * 
     */
    public function addRuleType($type) 
    {
        if (!$this->isSupportedRule($type)) {
            trigger_error('Unsupported rule type', E_USER_WARNING);
        }
        if (!isset($this->rules[$type])) {
            $this->rules[$type] = array();
            $this->lastAddedRule = $type;
        }
        
        return $this;
    }
    
    /**
     * Adds field(s)to last added rule or rule given with second $type parameter
     * @param mixed $field  field name from HTML or arrays oh fields name from HTML
     * @param string $type for supported validation rules see getSupportedRules method
     * @return \SimpleFormValidator
     */
    public function addRuleField($field, $type = null) 
    {
        $rule = $this->lastAddedRule;
        if (!is_null($type)) {
            if ($this->isSupportedRule($type)) {
                $rule = $type;
            } else {
                trigger_error('Unsupported rule type', E_USER_WARNING);    
                return null;
            }
        }
        
        if (!isset($this->rules[$rule])) {
            $this->rules[$rule] = array();
        }
        
        if (is_array($field)) {
            $this->rules[$rule] = array_merge($this->rules[$rule], $field);
        } else {
            $this->rules[$rule][] = $field;
        }
        
        return $this;
    }
    
    /**
     * Returns all supported validation rule types
     * @return array 
     */
    public function getSupportedRules() 
    {
        return $this->supportedRules;
    }
    
    /**
     * 
     * @param array $data
     * @return \SimpleFormValidator
     */
    public function setFormData($data) 
    {
        foreach($data as $field => $value) {
            $this->setValue($field, $value);
        }
        
        return $this;
    }

    /**
     * Prepares validator to beginning state
     *
     * @param  array $data data array to validate.. usually $_POST
     */
    protected function prepareValidate($data = null)
    {
        $_SESSION[$this->sessionNS]['isSent'] = true;
        $this->clearErrorsAndData();

        if (!is_null($data) && is_array($data)) {
            $this->setFormData($data);
        }
    }
    
    /**
     * Validates form inputs by set of rules and set its isSent state to true
     * @param array $data data array to validate.. usually $_POST
     * @return boolean 
     */
    public function validate($data = null)            
    {
        $this->prepareValidate($data);
        
        $values = $this->getValues();
        foreach ($this->getRules() as $rule => $fields) {
            foreach ($fields as $field) {
                $field = $this->normalizeFieldName($field);
                if ($rule == self::RULE_REQUIRED) {
                    if (!isset($values[$field]) || empty($values[$field]) || (is_scalar($values[$field]) && trim($values[$field]) == '')) {
                        $this->setError($field, $this->messages[self::RULE_REQUIRED]);
                    }
                } elseif ($rule == self::RULE_TELEPHONE) {
                    if (!$this->isTelephone($values[$field])) {
                        $this->setError($field, $this->messages[self::RULE_TELEPHONE]);
                    }
                } elseif ($rule == self::RULE_NUMERIC) {
                    if (!is_numeric($values[$field])) {
                        $this->setError($field, $this->messages[self::RULE_NUMERIC]);
                    }
                } elseif ($rule == self::RULE_EMAIL) {
                    if (!filter_var($values[$field], FILTER_VALIDATE_EMAIL)) {
                       $this->setError($field, $this->messages[self::RULE_EMAIL]);
                    }
                } elseif ($rule == self::RULE_EMPTY) {
                    if (!empty($values[$field])) {
                       $this->setError($field, $this->messages[self::RULE_EMPTY]);
                    }
                } elseif ($rule == self::RULE_CHECK) {
                    if (!isset($values[$field])) {
                        $this->setError($field, $this->messages[self::RULE_CHECK]);
                    }
                }
            }
        }

        if (!$this->hasError()) {
            $this->clear();
            return true;
        }

        return false;        
    }


    
    /**
     * Clears all saved SimpleForm data
     */
    protected function clear()
    {        
        $this->clearErrorsAndData();
        $_SESSION[$this->sessionNS]['isSent'] = false;

        $this->rules         = array();
        $this->lastAddedRule = '';
    }

    /**
     * Clears all errors and data. This method is called before each validation call
     */
    private function clearErrorsAndData()
    {
        $_SESSION[$this->sessionNS]['errors'] = array();
        $_SESSION[$this->sessionNS]['values'] = array();
    }
    
    /**
     * Telephone numbers can contains only this chars: 0-9, white chars (spaces) and can starts by +
     * @param string $telephone
     * @return boolean 
     */
    private function isTelephone($telephone)
    {
        return (bool)preg_match('~^\+?[0-9\s]*$~', $telephone);
    }
    
    /**
     * Checks if given type is supported validation rule
     * @param string $type name of validation rule
     * @return boolean 
     */
    private function isSupportedRule($type) 
    {
        return in_array(strtolower($type), $this->getSupportedRules());
    }

    private function normalizeFieldName($field)
    {
        if (preg_match('~(?P<field>.*)\[\]$~', $field, $matches)) {
            $field = $matches['field'];
        }

        return $field;
    }
}
